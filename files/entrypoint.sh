#!/bin/sh
set -eu
if [ "$MAX_BODY_SIZE_MB" -le "0" ]
then
   MIDDLEWARE=""
   MAX_BODY_SIZE_BYTES=-1
else
   MIDDLEWARE=reverseProxyMiddleware
   MAX_BODY_SIZE_BYTES=$((MAX_BODY_SIZE_MB*1024*1024))
fi

# the "${VAR+1}" check triggers even if VAR is set to empty string
# (which might be a desired value since an empty string header value
# counts as header removal instruction in the Traefik middleware)
if [ -n "${X_FORWARDED_PROTO+1}" ]
then
   if [ -n "${MIDDLEWARE}" ]
   then
      MIDDLEWARE="${MIDDLEWARE},rewriteForwardedProtoHeader"
   else
      MIDDLEWARE=rewriteForwardedProtoHeader
   fi
else
   X_FORWARDED_PROTO=""
fi

#mkdir -m 766 /etc/traefik

READ_TIMEOUT=${READ_TIMEOUT:-60}

sed -e "s/\${SERVICE_PORT}/${SERVICE_PORT}/" \
    -e "s/\${READ_TIMEOUT}/${READ_TIMEOUT}/" \
    -e "s/\${METRICS_PORT}/${METRICS_PORT}/" /templates/traefik.template > /config/traefik.yml

if [ -n "${TCP_MODE+1}" ]; then
   sed -e "s/\${LOCAL_PORT}/${LOCAL_PORT}/" /templates/tcp-dynamic.template > /config/dynamic.yml
else
   sed -e "s/\${LOCAL_PORT}/${LOCAL_PORT}/" \
       -e "s/\${MIDDLEWARE}/${MIDDLEWARE}/" \
       -e "s/\${X_FORWARDED_PROTO}/${X_FORWARDED_PROTO}/" \
       -e "s/\${MAX_BODY_SIZE_BYTES}/${MAX_BODY_SIZE_BYTES}/" /templates/dynamic.template > /config/dynamic.yml
fi

exec traefik --configfile /config/traefik.yml
