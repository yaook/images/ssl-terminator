##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM traefik:v3.3.2

ENV METRICS_PORT=8002
ENV MAX_BODY_SIZE_MB=100

RUN set -eux ; \
    apk update ; \
    apk add --no-cache curl

RUN mkdir -m 777 config

USER 2500015
COPY --chown=2500015:2500015 files/traefik.template files/dynamic.template files/tcp-dynamic.template /templates/
COPY files/entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
