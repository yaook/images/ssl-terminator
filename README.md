# Tiny ssl terminator

This is a ssl terminating reverse proxy for servers listening on 127.0.0.1

## Parameters

* Environment variable `SERVICE_PORT`: The port on which the ssl terminator will listen
* Environment variable `LOCAL_PORT`: The port to which the ssl terminator should forward the connection to. Must be listening on 127.0.0.1
* Environment variable `READ_TIMEOUT`: The maximum duration in seconds for reading an incoming request for TCP connections (default 60).
* Environment variable `METRICS_PORT`: The port to scrap the metrics from the ssl terminator to prometheus. The default port is: 8002
* Optional Environment variable `MAX_BODY_SIZE_MB`: The maximum upload buffer size in mebibytes. Defaults to 100MB. Uploads larger then that are rejected. If this is set to "0", no upload size buffer is set. This is needed for e.g. uploading Glance Images.
* Optional Environment variable `X_FORWARDED_PROTO`: If set, adds a middleware to the request handling that rewrites/adds the `X-Forwarded-Proto` header set to the value of the environment variable before passing requests to the backend.
* Environment variable `TCP_MODE`: If set, the ssl-terminator will run in tcp mode
* Path `/data/tls.crt` and `/data/tls.key`: The certificate to present to the client (possibly including a chain) and its private key

## License

[Apache 2](LICENSE.txt)
